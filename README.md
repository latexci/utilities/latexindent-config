# Latexindent config

A simple [latexindent][latexindent] config that tries to be useful to convert old TeX documents to a reasonable good style.
As always with `latexindent`, make backups and use this at your own risk.

Run with
```
latexindent -w -s -m -rv filename.tex
```
or refer to the latexindent manual for further details.

# License
This is just the default latexindent config, where parameters have been modified,
the original file is part of the [latexindent distribution][latexindent] and published under the [GPLv3 license][gplv3]. Thus, this repository is itself GPLv3-licensed.

[latexindent]: https://ctan.org/pkg/latexindent
[gplv3]: https://www.gnu.org/licenses/gpl-3.0.en.html
